﻿namespace Consumer
{
    using System;

    public class ConsumerError : EasyNetQ.SystemMessages.Error
    {
        public string Queue { get; set; }
        public DateTime UtcDateTime { get; set; }
    }
}