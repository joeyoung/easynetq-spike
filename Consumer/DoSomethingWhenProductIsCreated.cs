﻿namespace Consumer
{
    using System;
    using EasyNetQ.AutoSubscribe;
    using Messages;

    public class DoSomethingWhenProductIsCreated : IConsume<ProductCreatedEvent>
    {
        public void Consume(ProductCreatedEvent message)
        {
//            Console.Out.WriteLine("Doing Someting with Product: " + message.Id);
            throw new Exception("Oops! I did it again.");
        }
    }
}