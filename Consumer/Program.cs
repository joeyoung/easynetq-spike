﻿namespace Consumer
{
    using System;
    using System.Reflection;
    using EasyNetQ;
    using EasyNetQ.AutoSubscribe;
    using EasyNetQ.Consumer;

    class Program
    {
        static void Main(string[] args)
        {
            var conventions = new SpikeConventions();

            var bus = RabbitHutch.CreateBus("host=localhost;username=guest;password=password", register =>
                {
                    register.Register<IConventions>(_ => conventions);
                    register.Register<IConsumerErrorStrategy>(sp => new ReQueueableErrorStrategy(sp.Resolve<IConnectionFactory>(), sp.Resolve<ISerializer>(), sp.Resolve<IEasyNetQLogger>(), sp.Resolve<IConventions>(), sp.Resolve<ITypeNameSerializer>()));
                });

           
            var autosubscriber = new AutoSubscriber(bus, "_")
                {
                    GenerateSubscriptionId = info => info.ConcreteType.FullName
                };

            autosubscriber.Subscribe(Assembly.GetExecutingAssembly());

            var errorQueueName = conventions.ErrorQueueNamingConvention;
            var queue = bus.Advanced.QueueDeclare(errorQueueName());

            bus.Advanced.Consume<ConsumerError>(queue, (message, info) =>
                {
                    var error = message.Body;
                    Console.Out.WriteLine("Error from queue: " + error.Queue);
                });
        }
    }
}
