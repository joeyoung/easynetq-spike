﻿namespace Consumer
{
    using EasyNetQ;

    public class SpikeConventions : Conventions
    {
        public SpikeConventions() : this(new TypeNameSerializer())
        {
            
        }

        private SpikeConventions(ITypeNameSerializer typeNameSerializer) : base(typeNameSerializer)
        {
            QueueNamingConvention = (messageType, subscriptionId) => subscriptionId;
            ErrorQueueNamingConvention = () => "Error_Queue";
        }
    }
}