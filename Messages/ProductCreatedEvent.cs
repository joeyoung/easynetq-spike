﻿namespace Messages
{
    using System;

    public class ProductCreatedEvent
    {
        public Guid Id { get; set; } 
    }
}