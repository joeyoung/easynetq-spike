﻿namespace Publisher
{
    using System;
    using System.Threading;
    using EasyNetQ;
    using Messages;

    internal class Program
    {
        private static IBus bus;

        private static void Main(string[] args)
        {
            bus = RabbitHutch.CreateBus("host=localhost;username=guest;password=password");

            Console.Out.WriteLine("Press Any Key to Send a Message");
            while (true)
            {
                Console.ReadLine();
//                PublishLotsOfEventsWithThreads();
                PublishEvent();
            }
        }

        private static void PublishLotsOfEventsWithThreads()
        {
            var thread1 = new Thread(PublishLotsOfEvents);
            var thread2 = new Thread(PublishLotsOfEvents);

            thread1.Start();
            thread2.Start();
        }

        private static void PublishLotsOfEvents()
        {
            for (var i = 0; i <= 100000; i++)
            {
                PublishEvent();
            }
        }

        private static void PublishEvent()
        {
            bus.Publish(new ProductCreatedEvent { Id = Guid.NewGuid() });
        }
    }
}