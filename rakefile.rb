
require 'rest_client'
require 'json'
require 'win32console'

Rake.application.options.trace = false

RABBIT = "http://localhost:15672/api"
VHOST = "/"

namespace :rabbit do
	
	namespace :delete do
		desc "Deletes all queues and exchanges from #{RABBIT}"
		task :all => ["rabbit:queue:delete_all", "rabbit:exchange:delete_all"]
	end
	
	namespace :vhost do
		desc "Get a list of virtual hosts from #{RABBIT}"
		task :list do
			vhosts = get_request("vhosts")
			vhosts.each do |h|
				puts h["name"]
			end
		end
	end
	
	namespace :exchange do
		desc "Get a list of exchanges from #{RABBIT}"
		task :list do
			exchanges = get_exchanges
			exchanges.each do |e|
				puts e["name"] 
			end
		end
				
		desc "Deletes all the exchanges in #{RABBIT}"
		task :delete_all do
			exchanges = get_exchanges
			exchanges.each do |e|
				name = e["name"] 
				path = "exchanges/#{sanitize(VHOST)}/#{sanitize(name)}"
								
				response = delete_request(path)
				if code_isvalid(response.code)
					puts "deleted #{name}".green
				else
					puts "could not delete #{name}".red
				end
			
			end
		end
	end
	
	namespace :queue do
	
		desc "Gets all the queues from #{RABBIT}"
		task :list do
			queues = get_queues
			queues.each do |q|
				puts q["name"]
			end
		end
		
		desc "Deletes all the queues from #{RABBIT}"
		task :delete_all do
			queues = get_queues
			queues.each do |q|
				name = q["name"]
				path = "queues/#{sanitize(VHOST)}/#{sanitize(name)}"
				
				response = delete_request(path)
				if code_isvalid(response.code)
					puts "deleted #{name}".green
				else
					puts "could not delete #{name}".red
				end
			end
		end
	end

	def get_queues()
		queues = get_request("queues/#{sanitize(VHOST)}")
		
		return queues
	end
	
	def get_exchanges()
		exchanges = get_request("exchanges/#{sanitize(VHOST)}")
		
		exchanges.delete_if do |e|
			name = e["name"]
			name.empty? || name.start_with?("amq")
		end
	end

	def sanitize(value)
		return value.gsub("/", "%2f").gsub("#", "%23")
	end
	
	def get_request(path)
		response = RestClient::Request.new(
						:method => :get,
						:url => "#{RABBIT}/#{path}",
						:user => "admin",
						:password => "password",
						:headers => {
							:accept => :json,
							:content_type => :json
							}
					).execute
					
		return JSON.parse(response.to_str)
	end
	
	def delete_request(path)
		response = RestClient::Request.new(
						:method => :delete,
						:url => "#{RABBIT}/#{path}",
						:user => "admin",
						:password => "password",
						:headers => {
							:accept => :json,
							:content_type => :json
							}
					).execute
					
		return response
	end
	
	def code_isvalid(code)
		return code >= 200 && code < 300
	end
	
	class String
	  { :reset          =>  0,
		:bold           =>  1,
		:dark           =>  2,
		:underline      =>  4,
		:blink          =>  5,
		:negative       =>  7,
		:black          => 30,
		:red            => 31,
		:green          => 32,
		:yellow         => 33,
		:blue           => 34,
		:magenta        => 35,
		:cyan           => 36,
		:white          => 37,
	  }.each do |key, value|
		define_method key do
		  "\e[#{value}m" + self + "\e[0m"
		end
	  end
	end
end